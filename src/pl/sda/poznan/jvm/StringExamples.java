package pl.sda.poznan.jvm;

import java.util.Scanner;

public class StringExamples {
    public static void main(String[] args) {
        String x1 = "Ala";
        String x2 = new String("Ala");

        Scanner scanner = new Scanner(System.in);
//       String x2 = scanner.next();
        if (x1 == x2) {
            System.out.println("rowne");
        } else {
            System.out.println("nie rowne");
        }

        System.out.println("EQUALS");
        if (x1.equals(x2)) {
            System.out.println("rowne");
        } else {
            System.out.println("nie rowne");
        }


        // String methods //
        String name = "Piotr";
        String result = name.substring(2, 4);
        System.out.println(name);
        System.out.println(result);

        System.out.println(name.indexOf("i"));
    }
}
